<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db = new SQLite3("barlog.db");
$result = $db->query("SELECT plus_key, minus_key FROM drinks");
$rel_keys = [];
while($row = $result->fetchArray(SQLITE3_ASSOC)) {
    $rel_keys[] = $row['plus_key'];
    $rel_keys[] = $row['minus_key'];
}
$rel_keys = json_encode($rel_keys);
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>BarChart: Control</title>
    <script src="jquery.js"></script>
    
    <style>
        body {
            background: rgb(30,30,30);
            color: rgb(255,255,255);
            font-family: sans-serif;
        }
        
        span.letter {
            text-transform: uppercase;
            font-family: monospace;
            font-weight: bold;
            border: solid 1px white;
            border-radius: 0.5em;
            padding: 0.4em;
            margin-right: 0.5em;
        }
        
        div.framed {
            line-height: 150%;
            padding-right: 2em;
            border: solid 2px;
            padding: 1em;
            margin: 0.5em;
            border-radius: 0.5em;
        }
        
        div.instruction {
            display: inline-block;
            
        }
        
        div#feedback {
            min-height: 1em;
            transition: background 0.1s;
        }
        
        h3 {
            margin-top: 0.25em;
        }
    </style>
    
    <script type="text/javascript">
        var rel_keys = <?php echo $rel_keys; ?>;
        
        $(document).keypress(function(e){
            console.log(e.charCode);
            if(rel_keys.indexOf(e.charCode) >= 0) {
                rel_pressed(e.charCode);
            }
        });
        
        function rel_pressed(pressed) {
            $.ajax({
                url: "push.php?code=" + pressed,
                success: function(data) {
                    positiveFeedback(data);
                },
                error: function() {
                    $("#feedback").text("AJAX Error");
                }
            });
        }
        
        function positiveFeedback(data) {
            $("#feedback").html(data);
            $("#feedback").css("background", "rgb(0,200,0)");
            setTimeout(function() {
                $("#feedback").css("background", "none");
            }, 100);
            
        }
        
    </script>
    
</head>

<body>
    
    <div class="framed">
        <h3>Server Feedback</h3>
        <div id="feedback"></div>
    </div>
    
    <?php
    $result = $db->query("SELECT * FROM drinks ORDER BY ordern ASC");
    
    while($row = $result->fetchArray(SQLITE3_ASSOC)) {
        ?>
        <div class="framed instruction" style="border-color: <?php echo $row['bar_color']; ?>;">
            <h3><?php echo $row['name']; ?></h3>
            <p class="instruction">
                <span class="letter"><?php echo $row['plus_letter']; ?></span> for +1 <?php echo $row['name']; ?>
            </p>
            
            <p>
                <span class="letter"><?php echo $row['minus_letter']; ?></span> for -1 <?php echo $row['name']; ?>
            </p>
        </div>
        <?php
    }
    ?>


</body>

</html>