<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db = new SQLite3("barlog.db");

//Informationen über einzelne Drinks speichern
$result = $db->query("SELECT id, volume_ps FROM drinks ORDER BY id");
$drinks = [];
while($row = $result->fetchArray(SQLITE3_ASSOC)) {
    $drink_ids[] = $row['id'];
    $drinks[$row['id']] = $row;
}

//Logbuch speichern
$result = $db->query("SELECT * FROM drinklog");
while($row = $result->fetchArray(SQLITE3_ASSOC)) {
    $drinklog[] = $row;
}
//var_dump($drink_ids);

//Gesamtanzahl aller und jedes einzelnen Drinks auf 0 setzen
$global_drinks = 0;
foreach($drink_ids as $drink_id) {
    $drinks[$drink_id]['total_served'] = 0;
}

//Gesamtanzahl aller und jedes einzelnen Drinks zusammenzählen: in ['total_served']
for($i = 0; $i < sizeof($drinklog); $i++) {
    $global_drinks += $drinklog[$i]['modulator'];
    $drinks[$drinklog[$i]['drink_id']]['total_served'] += $drinklog[$i]['modulator'];
}

//Prozentsatz und Gesamtvolumen der einzelnen Drinks berechnen
//Zusätzlich Array aller Drink-Anzahlen erstellen
$all_drink_sums = [];
foreach($drink_ids as $drink_id) {
    $drinks[$drink_id]['volume'] = round($drinks[$drink_id]['total_served'] * $drinks[$drink_id]['volume_ps'], 2);
    //Division durch 0 ausschliessen
    if($global_drinks == 0) $drinks[$drink_id]['percentage'] = 0;
    else $drinks[$drink_id]['percentage'] = round(($drinks[$drink_id]['total_served'] / $global_drinks) *100, 2);
    $all_drink_sums[] = $drinks[$drink_id]['total_served'];
}

//Meist-servierter Drink suchen
$max_drink_sum = max($all_drink_sums);

//relative Balkenhöhe zum maximalen Drink berechnen
foreach($drink_ids as $drink_id) {
    //Division durch 0 ausschliessen
    if($max_drink_sum == 0) $drinks[$drink_id]['bar_percentage'] = 0;
    else $drinks[$drink_id]['bar_percentage'] = round(($drinks[$drink_id]['total_served'] / $max_drink_sum) *100);
}

//Daten zusammenfügen
$final_result['ids'] = $drink_ids;
$final_result['data'] = $drinks;
echo json_encode($final_result);

//var_dump($drinks);

?>