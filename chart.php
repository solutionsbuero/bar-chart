<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$db = new SQLite3("barlog.db");
$result = $db->query("SELECT * FROM drinks ORDER BY ordern");
$drinks = [];
while($row = $result->fetchArray(SQLITE3_ASSOC)) {
    $drinks[] = $row;
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>BarChart</title>
     <script src="jquery.js"></script>
     
    <style>
        
        body {
            background: rgb(30,30,30);
            color: rgb(255,255,255);
            font-family: monospace;
        }
        
        h1 {
            text-align: center;
            font-weight: normal;
        }
        
        span.bar_span {
            /*min-height: 1em;*/
            min-width: 2em;
            display: inline-block;
        }
        
        table#bartable {
            width: 90%;
            height: 90vh;
            margin: auto;
            border-collapse: collapse;
        }
        
        table#bartable td {
            width: <?php echo floor(100/(sizeof($drinks) +1)); ?>%;
            border-collapse: collapse;
            /*border: solid 1px white;*/
            border: none;
        }
        
        td.bar, td.name, td.percentage, td.units, td.volume {
            text-align: center;
        }
        
        td.bar {
            height: 80vh;
            vertical-align: bottom;
        }
        
        td.lefty {
            text-align: right !important;
        }
    </style>
    
    <script type="text/javascript">
        function get_data() {
            $.ajax({
                url: "pull.php",
                success: function(data) {
                    console.log(JSON.parse(data));
                    plot_data(JSON.parse(data));
                },
                error: function() {
                    console.log("AJAX Error");
                }
            });
        }
        
        function plot_data(data) {
            console.log(data.ids);
            data.ids.forEach(function(id) {
                $("#bar_span_" + id).css("height", data.data[id].bar_percentage + "%");
                $("#units_" + id).text(data.data[id].total_served);$("#units_" + id).text(data.data[id].total_served);
                $("#percentage_" + id).text(data.data[id].percentage);
                $("#volume_" + id).text(data.data[id].volume);
            });
        }
        
        function start_plotting() {
            setInterval(get_data, 2000);
        }
    </script>
    
</head>

<body onload="start_plotting();">
    <h1>bar_chart://drinking.data</h1>
    
    <table id="bartable">
        <tr>
            <td class="bar lefty"></td>
            <?php
            for($i = 0; $i < sizeof($drinks); $i++) {
                ?>
                <td class="bar">
                    <span class="bar_span" id="bar_span_<?php echo $drinks[$i]['id']; ?>" style="background: <?php echo $drinks[$i]['bar_color']; ?>"></span>
                </td>
                <?php
            }
            ?>
        </tr>
        <tr>
            <td class="name lefty">Drink:</td>
            <?php
            for($i = 0; $i < sizeof($drinks); $i++) {
                ?>
                <td class="name">
                    <?php echo $drinks[$i]['name']; ?>
                </td>
                <?php
            }
            ?>
        </tr>
        <tr>
            <td class="units lefty">Units served:</td>
            <?php
            for($i = 0; $i < sizeof($drinks); $i++) {
                ?>
                <td class="units">
                    <span id="units_<?php echo $drinks[$i]['id']; ?>">0</span> Units
                </td>
                <?php
            }
            ?>
        </tr>
        <tr>
            <td class="percentage lefty">Percentage (Units):</td>
            <?php
            for($i = 0; $i < sizeof($drinks); $i++) {
                ?>
                <td class="percentage">
                    <span id="percentage_<?php echo $drinks[$i]['id']; ?>">0</span> %
                </td>
                <?php
            }
            ?>
        </tr>
        <tr>
            <td class="volume lefty">Volume:</td>
            <?php
            for($i = 0; $i < sizeof($drinks); $i++) {
                ?>
                <td class="volume">
                    <span id="volume_<?php echo $drinks[$i]['id']; ?>">0</span> l
                </td>
                <?php
            }
            ?>
        </tr>
    </table>
    
</body>

</html>