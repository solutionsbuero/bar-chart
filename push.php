<?php
$code = $_GET['code'];
$db = new SQLite3("barlog.db");
$result = $db->query("SELECT id, plus_key, minus_key, name FROM drinks");
$now = time();

while($row = $result->fetchArray(SQLITE3_ASSOC)) {
    if($code == $row['plus_key']) {
        $name = $row['name'];
        $drink_id = $row['id'];
        $modulator = 1;
    }
    elseif($code == $row['minus_key']) {
        $name = $row['name'];
        $drink_id = $row['id'];
        $modulator = -1;
    }
}

if(!isset($drink_id)) {
    die("Error: key not mapped to a drink.");
}

$dbpush = $db->query("INSERT INTO drinklog (drink_id, timestamp, modulator) VALUES ('$drink_id', '$now', '$modulator')");

if($dbpush) {
    if($modulator > 0) $fb_verb = "added";
    if($modulator < 0) $fb_verb = "substracted";
    echo $fb_verb." ".abs($modulator)." ".$name;
}

else {
    echo $db->lastErrorMsg();
}
?>